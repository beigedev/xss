var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var fs = require('fs');
var lineReader = require('line-reader');
var uuid = require('node-uuid');
let cookieParser = require('cookie-parser');   
app.use(cookieParser()); 

app.get('/', function(req, res) {
	res.cookie("cookieUUID", uuid.v4());

  	app.use(express.static(path.join(__dirname)));
  	res.sendFile(path.join(__dirname, '../chat', 'index.html'));
});

io.on('connection', function(socket){ 
	socket.on('chatMessage', function(from, msg) {
		if (from !== 'System') {
			fs.appendFile("messages.txt", from + "±" + msg + "\n", function (err) {
        		if (err) console.log(err);
    		});
		} else {
			lineReader.eachLine('messages.txt', function(line, last) {
				socket.emit('chatMessage', line.split('±')[0], line.split('±')[1]); 
			});
		}

    	io.emit('chatMessage', from, msg);
  	});

  	socket.on('notifyUser', function(user) {
    	io.emit('notifyUser', user);
  	});
});

http.listen(8080, function() {
	console.log('chat running at localhost:8080');
});