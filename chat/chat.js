var socket = io();

$(document).ready(function() {
  	var name = generateId();

  	$('#user').val(name);
  	socket.emit('chatMessage', 'System', '<b>' + name + '</b> has joined the discussion');
});

function submitFunction() {
  	var from = $('#user').val();
  	var message = $('#m').val();
  	
  	if (message != '') {
  		socket.emit('chatMessage', from, message);
	}

	$('#m').val('').focus();
  	return false;
}

function notifyTyping() { 
  	var user = $('#user').val();
  	
  	socket.emit('notifyUser', user);
}

socket.on('chatMessage', function(from, msg) {
  	var me = $('#user').val();
  	var color = (from == me) ? '#00AE90' : '#D82B0E';
  	var from = (from == me) ? 'Me' : from;
  	$('#messages').append('<li><b style="color:' + color + '">' + from + '</b>: ' + msg + '</li>');
});

socket.on('notifyUser', function(user) {
  	var me = $('#user').val();
  	
  	if (user != me) {
    	$('#notifyUser').text(user + ' is typing...');
  	}
  	
  	setTimeout(function() {
  		$('#notifyUser').text('');
  	}, 2500);
});

function generateId() {
  	var text = "";
  	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  	for (var i = 0; i < 5; i++) {
    	text += possible.charAt(Math.floor(Math.random() * possible.length));
  	}

  	return text;
}